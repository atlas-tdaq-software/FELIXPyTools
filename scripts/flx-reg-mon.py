#!/usr/bin/env tdaq_python

import logging
import argparse
import subprocess
import sys
from time import sleep
import ispy
import ers
from ipc import IPCPartition
from libflxcard_py import flxcard

registers = {
    "GT_AUTO_RX_RESET_CNT" : {'name': "GT_AUTO_RX_RESET_CNT_##_VALUE", 'devices' : [0], 'links' : [(0,23)]},
    "GBT_ALIGNMENT_DONE" : {'name': "GBT_ALIGNMENT_DONE", 'devices' : [0], 'links' : [(0, 0)]},
    "GBT_ALIGNMENT_LOST" : {'name': "GBT_ALIGNMENT_LOST_ALIGNMENT_LOST", 'devices' : [0], 'links' : [(0, 0)]},
    "GBT_PLL_LOL_LATCHED_QPLL" : {'name': "GBT_PLL_LOL_LATCHED_QPLL_LOL_LATCHED", 'devices' : [0], 'links' : [(0, 0)]},
    "GBT_PLL_LOL_LATCHED_CPLL" : {'name': "GBT_PLL_LOL_LATCHED_CPLL_LOL_LATCHED", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_CDRLOCK_MONITOR_ADN_LOL_LATCHED" : {'name': "TTC_CDRLOCK_MONITOR_ADN_LOL_LATCHED", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_CDRLOCK_MONITOR_ADN_LOS_LATCHED" : {'name': "TTC_CDRLOCK_MONITOR_ADN_LOS_LATCHED", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_CDRLOCK_MONITOR_CDRLOCK_LOST" : {'name': "TTC_CDRLOCK_MONITOR_CDRLOCK_LOST", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_DEC_MON_TTC_BIT_ERR" : {'name': "TTC_DEC_MON_TTC_BIT_ERR", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_CDRLOCK_MONITOR_CDRLOCKED" : {'name': "TTC_CDRLOCK_MONITOR_CDRLOCKED", 'devices' : [0], 'links' : [(0, 0)]},
    "HK_CTRL_FMC_SI5345_LOL_LATCHED" : {'name': "HK_CTRL_FMC_SI5345_LOL_LATCHED", 'devices' : [0], 'links' : [(0, 0)]},
    "MMCM_MAIN_LOL_LATCHED" : {'name': "MMCM_MAIN_LOL_LATCHED", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_BCR_PERIODICITY_MONITOR_VALUE" : {'name': "TTC_BCR_PERIODICITY_MONITOR_VALUE", 'devices' : [0], 'links' : [(0, 0)]},
    "ELINK_REALIGNMENT_STATUS" : {'name': "ELINK_REALIGNMENT_STATUS_##", 'devices' : [0, 1], 'links' : [(0, 11), (0,11)]},
    "GT_FEC_ERR_CNT" : {'name': "GT_FEC_ERR_CNT_##", 'devices' : [0], 'links' : [(0, 23)]},
}

clear_registers = {
    "GT_AUTO_RX_RESET_CNT" : {'name': "GT_AUTO_RX_RESET_CNT_##_CLEAR", 'devices' : [0], 'links' : [(0,23)]},
    "GBT_ALIGNMENT_LOST_CLEAR" : {'name': "GBT_ALIGNMENT_LOST_CLEAR", 'devices' : [0], 'links' : [(0, 0)]},
    "TTC_CDRLOCK_MONITOR_CLEAR" : {'name': "TTC_CDRLOCK_MONITOR_CLEAR", 'devices' : [0], 'links' : [(0, 0)]},
    "HK_CTRL_FMC_CLEAR" : {'name': "HK_CTRL_FMC_CLEAR", 'devices' : [0], 'links' : [(0, 0)]},
    "MMCM_MAIN_CLEAR" : {'name': "MMCM_MAIN_CLEAR", 'devices' : [0], 'links' : [(0, 0)]},
    "GBT_PLL_LOL_LATCHED_CLEAR" :{'name': "GBT_PLL_LOL_LATCHED_CLEAR", 'devices' : [0], 'links' : [(0, 0)]},
}


def expand_links(rdata, device):
    """Creates a list of per-link registers expanding the register name"""
    regs = []
    rname = rdata['name']
    links = rdata['links'][device]
    for l in range(links[0], links[1]+1):
        link_number = "{:02d}".format(l)
        regs.append(rname.replace('##', str(link_number)))
    return regs



class dut:

    def __init__(self, logger, card_number=0):
        """Class representing one FLX card.

        Both devices are opened.
        """
        self.logger = logger
        self.dev = [flxcard(), flxcard()]
        flxcard.card_open(self.dev[0], 2*card_number, flxcard.LOCK.NONE.value)
        flxcard.card_open(self.dev[1], (2*card_number)+1, flxcard.LOCK.NONE.value)

    def __del__(self):
        flxcard.card_close(self.dev[0])
        flxcard.card_close(self.dev[1])

   
    def read_registers(self, rkey):
        """Read register values from FLX card

        The function returns a list of lists structured as
        [[values for device 0], [values for device 1]]
        """
        values = [[], []]
        rdata = registers[rkey]
        for device in rdata['devices']:
            device_values = []
            regs = expand_links(rdata, device)
            for r in regs:
                try:
                    self.logger.debug("Retrieving %s from device %u", r, device)
                    device_values.append(flxcard.cfg_get_option(self.dev[device], r))
                except Exception as ex:
                    self.logger.error("Error in retrieving %s from device %u. %s", r, device, str(ex))
                    device_values = []
            values[device] = device_values
        return values;

    def reset_registers(self):
        """Reset register values from FLX card

        The function writes a 1 and then a 0 on the
        registers defined in the reset_registers global variable
        """
        for rkey in clear_registers:
            rdata = clear_registers[rkey]
            for device in rdata['devices']:
                regs = expand_links(rdata, device)
                for r in regs:
                    try:
                        self.logger.debug("Resetting %s from device %u", r, device)
                        flxcard.cfg_set_option(self.dev[device], r, 1)
                        flxcard.cfg_set_option(self.dev[device], r, 0)
                    except Exception as ex:
                        self.logger.error("Error writing on %s from device %u. %s", r, device, str(ex))


class publisher:

    def __init__(self, config):
        """IS publisher class"""

        self.conf = config
        self.hostname = subprocess.check_output('hostname')
        self.hostname = self.hostname.decode(sys.stdout.encoding).split('.cern.ch')[0]
        self.tdaq_app_name = 'flx-reg-mon-'+self.hostname.split('.cern')[0]
        self.sleeptime = self.conf['INTERVAL_S']
        self.cards = []
        self.setup_logger()

        detected_cards = int(flxcard.number_of_devices() / 2)
        if detected_cards ==  0:
            self.logger.error("No FELIX devices detected on %s", self.hostname)
            raise Exception("No FELIX devices detected.")
        else:
            for c in range(0, detected_cards):
                self.cards.append(dut(self.logger, card_number = c))

        self.ipc_partition = IPCPartition(self.conf['TDAQ_PARTITION'])
        if not self.ipc_partition.isValid():
            self.logger.fatal("Partition %s is invalid", self.conf['TDAQ_PARTITION'])
            raise Exception("Invalid partition.") 

    def setup_logger(self):
        """ERS logging"""
        self.logger = logging.getLogger('root')
        self.logger.setLevel(logging.INFO)
        self.ers_handler = ers.LoggingHandler()
        self.logger.addHandler(self.ers_handler)
        self.logger.debug("flx-reg-mon of %s reporting to %s partition.", self.hostname, self.conf['TDAQ_PARTITION'])


    def publish_data(self):
        is_id_root = f"{self.conf['TDAQ_IS_SERVER']}.{self.tdaq_app_name}"

        for c, card in enumerate(self.cards):
            # reset registers at start
            card.reset_registers()

        while True:
            # loop over detected cards
            for c, card in enumerate(self.cards):
                # loop over registers
                for r in registers.keys():
                    is_id = f"{is_id_root}.c{str(c)}.{r}"
                    try:
                        is_data = ispy.ISObject(self.ipc_partition, is_id, "FELIX_register")
                    except Exception as ex:
                        self.logger.error("Error retrieving ISObject: %s", str(ex))
                    data = card.read_registers(r) 
                    is_data.device0 = data[0]
                    is_data.device1 = data[1]
                    try: 
                        is_data.checkin()
                    except Exception as ex:
                        self.logger.error("Error publishing stats: %s", str(ex))
                # reset counters
                card.reset_registers()
            sleep(self.sleeptime)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Publish to IS the value of FELIX card registers")
    parser.add_argument("-p", type=str, default="FelixRegisterMonitor", help="Partition to publish to.")
    parser.add_argument("-s", type=str, default="FELIXDEBUG", help="ISserver to publish to.")
    parser.add_argument("-t", type=int, default=5, help="Statistics publication interval")
    args = parser.parse_args()

    config = {
        'TDAQ_PARTITION': args.p,
        'TDAQ_IS_SERVER' : args.s,
        'INTERVAL_S' : args.t,
    }
 
    p  = publisher(config)
    p.publish_data()
