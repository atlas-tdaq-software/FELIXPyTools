# FELIXPyTools
This repository contains Python scripts to monitor or debug FELIX.

### Content

#### flx-reg-mon
This script publishes on IS the values of FELIX registers.
The register are defined in `flx-reg-mon.py` while the schema is register agnostic. 

Note: PYTHONPATH needs to include the library directory of a FELIX release.
